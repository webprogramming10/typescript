let w : unknown = 1;
w= "string";
w ={
    runAnonExistentMethod: () =>{
        console.log("I think therefore I am");
    }

} as { runAnonExistentMethod:() => void}
if(typeof w == 'object' && w!== null){
    (w as {runAnonExistentMethod : () => void}).runAnonExistentMethod();
}